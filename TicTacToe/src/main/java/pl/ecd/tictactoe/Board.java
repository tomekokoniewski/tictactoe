package pl.ecd.tictactoe;

/**
 * @author tomek
 */
public class Board {

    
   private int[][] b;

    Board(int size) {
        this.b = new int[size][size];
    }


    public int[][] getBoard() {
        return b;
    }

}
