package pl.ecd.tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * class for communication with user
 * @author tomek
 */

public class AskUser {

    static Scanner scanner = new Scanner(System.in);

    private static void positiveInteger() {
        System.out.println("Data should be a positive integer");
    }

    static int borderSize() {
        boolean isInputCorrect = false;

        System.out.println("Please input a number represents border's size");
        int size = 0;

        while (!isInputCorrect) {
            try {
                size = scanner.nextInt();
                if (size < 0) {
                    positiveInteger();
                } else {

                    isInputCorrect = true;
                }
            } catch (InputMismatchException e) {
                positiveInteger();
                scanner.next();
            }
        }
        return size;
    }
    
    
    
}
